//
//  RaindropViewController.swift
//  Raindrop
//
//  Created by Dylan Law Wai Chun on 12/07/2016.
//  Copyright © 2016 Dylan. All rights reserved.
//

import UIKit

class RaindropViewController: UIViewController {
    var raindrops: [UILabel] = []
    var animator: UIDynamicAnimator?
    var gravity: UIGravityBehavior?
    let screenSize: CGRect = UIScreen.main.bounds

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for _ in 0...Int(screenSize.width/4) {
            var raindrop: UILabel!
            let rainX: CGFloat = CGFloat(arc4random_uniform(UInt32(screenSize.width)))
            print(screenSize.width)
            var rainY: CGFloat = -1 * CGFloat(arc4random_uniform(500))
            repeat {
                rainY = -1 * CGFloat(arc4random_uniform(500))
            } while(rainY >= -70 )
            raindrop = UILabel(frame: CGRect(x: rainX, y: rainY, width: 30, height: 30))
            raindrop.text = "💧"
            
            raindrops.append(raindrop)
            self.view.addSubview(raindrop)
        }
    }

    
    @IBAction func letRainFall(_ sender: AnyObject) {
        animateRainDrop()
    }
    
    func resetRaindrop(_ raindrop: UILabel) {
        
        let rainX: CGFloat = CGFloat(arc4random_uniform(UInt32(screenSize.width)))
        var rainY: CGFloat = -1 * CGFloat(arc4random_uniform(500))
        repeat {
            rainY = -1 * CGFloat(arc4random_uniform(500))
        } while(rainY >= -70 )
        raindrop.frame.origin.x = rainX;
        raindrop.frame.origin.y = rainY;
        
    }
    
    func animateRainDrop() {
        for raindrop in raindrops {
            let randomDelay: TimeInterval = TimeInterval(arc4random_uniform(10)/2)
            let randomScale: CGFloat = CGFloat(1 + arc4random_uniform(15)/5)
            raindrop.transform = CGAffineTransform.identity.scaledBy(x: randomScale , y: randomScale)
            self.resetRaindrop(raindrop)
            let randomAnimation: TimeInterval = Double(2 + (5/randomScale))
            UIView.animate(withDuration: randomAnimation, delay: randomDelay, options: UIViewAnimationOptions.repeat, animations: {
                raindrop.frame.origin.y += self.screenSize.height + 500
            
                }, completion: { (completed: Bool) in
                    print("Rainfall Complete")
            })
            
        }
    }
 
}
